build:
	: build not needed
install:
	mkdir -p $(DESTDIR)/etc/init.d/ || true
	mkdir -p $(DESTDIR)/etc/conf.d/ || true
	mkdir -p $(DESTDIR)/etc/X11/ || true
	mkdir -p $(DESTDIR)/usr/bin/ || true
	install -m 755 display-manager-setup.initd $(DESTDIR)/etc/init.d/display-manager-setup
	install -m 755 display-manager.initd-r1 $(DESTDIR)/etc/init.d/display-manager
	install xdm.initd $(DESTDIR)/etc/init.d/xdm
	install startDM-r1 $(DESTDIR)/usr/bin/startDM
	ln -s ../../usr/bin/startDM $(DESTDIR)/etc/X11/startDM.sh

